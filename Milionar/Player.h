#pragma once
#include "Milionar.h"

class Player
{
private:
	QString name;
	int score = 0;
public:
	Player();
	Player(QString n);
	~Player();
	void scoreUpdate(int change);
};

