/********************************************************************************
** Form generated from reading UI file 'Milionar.ui'
**
** Created by: Qt User Interface Compiler version 5.9.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MILIONAR_H
#define UI_MILIONAR_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MilionarClass
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_6;
    QSpacerItem *horizontalSpacer_4;
    QLabel *label_4;
    QComboBox *comboBox_2;
    QPushButton *pushButton_8;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLineEdit *lineEdit;
    QLabel *label_2;
    QDoubleSpinBox *doubleSpinBox;
    QLabel *label_3;
    QComboBox *comboBox;
    QCheckBox *checkBox;
    QPushButton *pushButton_4;
    QPushButton *pushButton_5;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QSpacerItem *horizontalSpacer_2;
    QHBoxLayout *horizontalLayout_2;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_2;
    QSpacerItem *verticalSpacer;
    QTextBrowser *QuestionField;
    QHBoxLayout *horizontalLayout_4;
    QRadioButton *radioButton;
    QRadioButton *radioButton_2;
    QRadioButton *radioButton_3;
    QRadioButton *radioButton_4;
    QHBoxLayout *horizontalLayout_5;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *pushButton_6;
    QPushButton *pushButton_7;
    QStatusBar *statusBar;
    QMenuBar *menuBar;

    void setupUi(QMainWindow *MilionarClass)
    {
        if (MilionarClass->objectName().isEmpty())
            MilionarClass->setObjectName(QStringLiteral("MilionarClass"));
        MilionarClass->resize(762, 468);
        centralWidget = new QWidget(MilionarClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        centralWidget->setMinimumSize(QSize(604, 353));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_4);

        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));

        horizontalLayout_6->addWidget(label_4);

        comboBox_2 = new QComboBox(centralWidget);
        comboBox_2->setObjectName(QStringLiteral("comboBox_2"));

        horizontalLayout_6->addWidget(comboBox_2);

        pushButton_8 = new QPushButton(centralWidget);
        pushButton_8->setObjectName(QStringLiteral("pushButton_8"));

        horizontalLayout_6->addWidget(pushButton_8);


        verticalLayout->addLayout(horizontalLayout_6);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout->addWidget(label);

        lineEdit = new QLineEdit(centralWidget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));

        horizontalLayout->addWidget(lineEdit);

        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout->addWidget(label_2);

        doubleSpinBox = new QDoubleSpinBox(centralWidget);
        doubleSpinBox->setObjectName(QStringLiteral("doubleSpinBox"));
        doubleSpinBox->setButtonSymbols(QAbstractSpinBox::NoButtons);
        doubleSpinBox->setDecimals(1);
        doubleSpinBox->setSingleStep(0.5);

        horizontalLayout->addWidget(doubleSpinBox);

        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        horizontalLayout->addWidget(label_3);

        comboBox = new QComboBox(centralWidget);
        comboBox->setObjectName(QStringLiteral("comboBox"));

        horizontalLayout->addWidget(comboBox);

        checkBox = new QCheckBox(centralWidget);
        checkBox->setObjectName(QStringLiteral("checkBox"));
        checkBox->setLayoutDirection(Qt::RightToLeft);

        horizontalLayout->addWidget(checkBox);

        pushButton_4 = new QPushButton(centralWidget);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));

        horizontalLayout->addWidget(pushButton_4);

        pushButton_5 = new QPushButton(centralWidget);
        pushButton_5->setObjectName(QStringLiteral("pushButton_5"));
        pushButton_5->setEnabled(false);

        horizontalLayout->addWidget(pushButton_5);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer);

        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setEnabled(false);

        horizontalLayout_3->addWidget(pushButton);

        pushButton_2 = new QPushButton(centralWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setEnabled(false);

        horizontalLayout_3->addWidget(pushButton_2);

        pushButton_3 = new QPushButton(centralWidget);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
        pushButton_3->setEnabled(false);

        horizontalLayout_3->addWidget(pushButton_3);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_2);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        verticalLayout_2 = new QVBoxLayout(groupBox);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);

        QuestionField = new QTextBrowser(groupBox);
        QuestionField->setObjectName(QStringLiteral("QuestionField"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(QuestionField->sizePolicy().hasHeightForWidth());
        QuestionField->setSizePolicy(sizePolicy);
        QuestionField->setAcceptDrops(true);

        verticalLayout_2->addWidget(QuestionField);


        horizontalLayout_2->addWidget(groupBox);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        radioButton = new QRadioButton(centralWidget);
        radioButton->setObjectName(QStringLiteral("radioButton"));
        radioButton->setEnabled(false);
        radioButton->setChecked(true);

        horizontalLayout_4->addWidget(radioButton);

        radioButton_2 = new QRadioButton(centralWidget);
        radioButton_2->setObjectName(QStringLiteral("radioButton_2"));
        radioButton_2->setEnabled(false);

        horizontalLayout_4->addWidget(radioButton_2);

        radioButton_3 = new QRadioButton(centralWidget);
        radioButton_3->setObjectName(QStringLiteral("radioButton_3"));
        radioButton_3->setEnabled(false);

        horizontalLayout_4->addWidget(radioButton_3);

        radioButton_4 = new QRadioButton(centralWidget);
        radioButton_4->setObjectName(QStringLiteral("radioButton_4"));
        radioButton_4->setEnabled(false);

        horizontalLayout_4->addWidget(radioButton_4);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_3);

        pushButton_6 = new QPushButton(centralWidget);
        pushButton_6->setObjectName(QStringLiteral("pushButton_6"));
        pushButton_6->setEnabled(false);

        horizontalLayout_5->addWidget(pushButton_6);

        pushButton_7 = new QPushButton(centralWidget);
        pushButton_7->setObjectName(QStringLiteral("pushButton_7"));
        pushButton_7->setEnabled(false);

        horizontalLayout_5->addWidget(pushButton_7);


        verticalLayout->addLayout(horizontalLayout_5);

        MilionarClass->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(MilionarClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MilionarClass->setStatusBar(statusBar);
        menuBar = new QMenuBar(MilionarClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 762, 21));
        MilionarClass->setMenuBar(menuBar);

        retranslateUi(MilionarClass);
        QObject::connect(pushButton_8, SIGNAL(clicked()), MilionarClass, SLOT(ActionChangeLanguage()));
        QObject::connect(pushButton_4, SIGNAL(clicked()), MilionarClass, SLOT(ActionNewGame()));
        QObject::connect(pushButton_6, SIGNAL(clicked()), MilionarClass, SLOT(ActionConfirm()));
        QObject::connect(pushButton_7, SIGNAL(clicked()), MilionarClass, SLOT(ActionSkip()));
        QObject::connect(pushButton_5, SIGNAL(clicked()), MilionarClass, SLOT(ActionEndGame()));
        QObject::connect(pushButton, SIGNAL(clicked()), MilionarClass, SLOT(Action5050()));
        QObject::connect(pushButton_2, SIGNAL(clicked()), MilionarClass, SLOT(Action5050()));
        QObject::connect(pushButton_3, SIGNAL(clicked()), MilionarClass, SLOT(Action5050()));

        QMetaObject::connectSlotsByName(MilionarClass);
    } // setupUi

    void retranslateUi(QMainWindow *MilionarClass)
    {
        MilionarClass->setWindowTitle(QApplication::translate("MilionarClass", "Milionar", Q_NULLPTR));
        label_4->setText(QApplication::translate("MilionarClass", "Jazyk: ", Q_NULLPTR));
        comboBox_2->clear();
        comboBox_2->insertItems(0, QStringList()
         << QApplication::translate("MilionarClass", "SK", Q_NULLPTR)
         << QApplication::translate("MilionarClass", "EN", Q_NULLPTR)
        );
        pushButton_8->setText(QApplication::translate("MilionarClass", "Zme\305\210 jazyk", Q_NULLPTR));
        label->setText(QApplication::translate("MilionarClass", "Meno:", Q_NULLPTR));
        label_2->setText(QApplication::translate("MilionarClass", "Sk\303\263re:", Q_NULLPTR));
        label_3->setText(QApplication::translate("MilionarClass", "N\303\241ro\304\215nos\305\245:", Q_NULLPTR));
        comboBox->clear();
        comboBox->insertItems(0, QStringList()
         << QApplication::translate("MilionarClass", "\304\275ahk\303\241", Q_NULLPTR)
         << QApplication::translate("MilionarClass", "Stredn\303\241", Q_NULLPTR)
         << QApplication::translate("MilionarClass", "\305\244a\305\276k\303\241", Q_NULLPTR)
        );
        checkBox->setText(QApplication::translate("MilionarClass", "N\303\241hodne", Q_NULLPTR));
        pushButton_4->setText(QApplication::translate("MilionarClass", "Nov\303\241 hra", Q_NULLPTR));
        pushButton_5->setText(QApplication::translate("MilionarClass", "Ukon\304\215i\305\245 hru", Q_NULLPTR));
        pushButton->setText(QApplication::translate("MilionarClass", "50:50", Q_NULLPTR));
        pushButton_2->setText(QApplication::translate("MilionarClass", "50:50", Q_NULLPTR));
        pushButton_3->setText(QApplication::translate("MilionarClass", "50:50", Q_NULLPTR));
        groupBox->setTitle(QApplication::translate("MilionarClass", "Ot\303\241zka:", Q_NULLPTR));
        radioButton->setText(QApplication::translate("MilionarClass", "(A)", Q_NULLPTR));
        radioButton_2->setText(QApplication::translate("MilionarClass", "(B)", Q_NULLPTR));
        radioButton_3->setText(QApplication::translate("MilionarClass", "(C)", Q_NULLPTR));
        radioButton_4->setText(QApplication::translate("MilionarClass", "(D)", Q_NULLPTR));
        pushButton_6->setText(QApplication::translate("MilionarClass", "Potvrdi\305\245", Q_NULLPTR));
        pushButton_7->setText(QApplication::translate("MilionarClass", "Presko\304\215i\305\245 ot\303\241zku", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MilionarClass: public Ui_MilionarClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MILIONAR_H
