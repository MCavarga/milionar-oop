﻿#include "Milionar.h"
#include <qDebug>
#include <QMessageBox>
#include <random>
#include <algorithm>

Player::Player()
{
}

void Player::setPlayer(QString n)
{
	name = n;
}


Player::~Player()
{
}

void Player::scoreUpdate(float change)
{
	score += change;
}

float Player::Score()
{
	return score;
}

void Player::NullScore()
{
	score = 0.0;
}



Question::Question()
{
	options = new QString[4];
}


Question::~Question()
{
	delete[] options;
}

void Question::setInfo(QString c, int a, QString A, QString B, QString C, QString D,int ff1, int ff2)
{
	content = c; anwser = a; fiftyFifty_indices[0] = ff1; fiftyFifty_indices[1] = ff2;
	options = new QString[4];
	options[0] = A; options[1] = B; options[2] = C; options[3] = D;
}

QString Question::Content()
{
	return content;
}

int Question::Anwser()
{
	return anwser;
}

QString Question::A()
{
	return options[0];
}

QString Question::B()
{
	return options[1];
}

QString Question::C()
{
	return options[2];
}

QString Question::D()
{
	return options[3];
}

int Question::fiftyFifty(int i)
{
	return fiftyFifty_indices[i];
}


Milionar::Milionar(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);

	// ================================== slovak questions easy ===================================

	questions_easySK[0].setInfo(QStringLiteral("Množinu mozno definovať ako skupinu objektov, ktoré: "), 
		2, QStringLiteral("(A) sú prvkami nejakej množiny."), 
		QStringLiteral("(B) obsahujú prvky."),
		QStringLiteral("(C) obsahujú každý prvok iba raz."),
		QStringLiteral("(D) obsahujú iba čísla."),2,3);

	questions_easySK[1].setInfo(QStringLiteral("Funkcia f: R -> R, pre ktorú existuje najmenšie také T>0, že f(x + T) = f(x) sa volá: "),
		3, QStringLiteral("(A) kubická"),
		QStringLiteral("(B) kvázi-periodická"),
		QStringLiteral("(C) nekonečná"),
		QStringLiteral("(D) periodická"),1,3);

	questions_easySK[2].setInfo(QStringLiteral("Majme matice A ~ B nad vektorovým priestorom V, potom existuje matica P taká, že B = P^(-1)AP. Matici P hovoríme: "),
		2, QStringLiteral("(A) reálna matica"),
		QStringLiteral("(B) inverzná matica"),
		QStringLiteral("(C) matica prechodu"),
		QStringLiteral("(D) súčinova matica"),2,3);

	questions_easySK[3].setInfo(QStringLiteral("Nech je Q matica nad vektorovým priestorom V a Q' k nej transponovaná matica. Potom ak Q'Q = Q Q' = I, matici Q hovoríme: "),
		0, QStringLiteral("(A) ortogonálna matica"),
		QStringLiteral("(B) inverzná matica"),
		QStringLiteral("(C) matica prechodu"),
		QStringLiteral("(D) súčinová matica"),0,2);

	questions_easySK[4].setInfo(QStringLiteral("Nech je (G,*) grupa. Prvok e z G taký, že e*x=x*e=x pre ľubovoľné x z G sa nazýva: "),
		3, QStringLiteral("(A) inverzný"),
		QStringLiteral("(B) nulový"),
		QStringLiteral("(C) súčinový"),
		QStringLiteral("(D) neutrálny"),0,3);

	questions_easySK[5].setInfo(QStringLiteral("Nech je f: M --> N zobrazenie. Hovoríme, že f je injektívne, ak:"),
		0, QStringLiteral("(A) sa každé dva rôzne prvky množiny M zobrazia na dva rôzne prvky množiny N."),
		QStringLiteral("(B) sa každé dva rôzne prvky množiny M zobrazia na jediný prvok množiny N."),
		QStringLiteral("(C) sa každý prvok množiny M zobrazí na každý prvok množiny N."),
		QStringLiteral("(D) sa ľubovoľný prvok z M zobrazí do spočítateľnej podmnožiny množiny N."),0,2);

	questions_easySK[6].setInfo(QStringLiteral("Aspoň dvakrát diferencovateľná funkcia f: M --> R nadobúda v bode p z M lokálne maximum ak: "),
		3, QStringLiteral("(A) pre x->p sa f(x) -> 0 "),
		QStringLiteral("(B) má v bode p nulovú deriváciu t.j.: f'(p) == 0"),
		QStringLiteral("(C) f'(x) > 0 pre x < p & f'(x) < 0 pre x > p & f''(p) == 0"),
		QStringLiteral("(D) f'(p) == 0 & f''(p) < 0"),1,2);

	questions_easySK[7].setInfo(QStringLiteral("Nech sú V,W vektorové priestory. Zobrazenie A:V-->W nazývame lineárne ak: "),
		1, QStringLiteral("(A) sa ľubovoľný vektor v z V zobrazí na priamku prechádzajúcu počiatkom."),
		QStringLiteral("(B) pre ľubovoľné dva vektory u,v z V a dva skaláry a,b platí: A(au+bv)=aA(u)+bA(v)."),
		QStringLiteral("(C) pre ľubovoľné dva vektory u,v z V a dva skaláry a,b platí: A(au+bv)=abA(u)A(v)."),
		QStringLiteral("(D) existuje inverzné zobrazenie A^(-1):W-->V také, že AA^(-1)=A^(-1)A=I."),0,1);

	questions_easySK[8].setInfo(QStringLiteral("Nech sú f a g diferencovateľné funkcie a g!=0 ani g'!=0, potom lim_{x->a} f(x)/g(x) = lim_{x->a} f'(x)/g'(x). Toto pravidlo nazývame: "),
		2, QStringLiteral("(A) podielové pravidlo"),
		QStringLiteral("(B) limitné pravidlo"),
		QStringLiteral("(C) L'Hospitalovo pravidlo"),
		QStringLiteral("(D) L'Hostinského pravidlo"),0,2);

	questions_easySK[9].setInfo(QStringLiteral("Nech f(x)=x*sin(x). Aká je primitívna funkcia k f? "),
		0, QStringLiteral("(A) sin(x)-x*cos(x) + C"),
		QStringLiteral("(B) neexistuje"),
		QStringLiteral("(C) cos(x) + C"),
		QStringLiteral("(D) 2*sin(x)*cos(x) + C"),0,2);

	// ================================== english questions easy ===================================

	questions_easyEN[0].setInfo(QStringLiteral("A set is a collection of objects that: "),
		2, QStringLiteral("(A) are elements of some set."),
		QStringLiteral("(B) contain elements."),
		QStringLiteral("(C) contain each element only once."),
		QStringLiteral("(D) contain just numbers."),2,3);

	questions_easyEN[1].setInfo(QStringLiteral("A function f: R -> R, for which there exists the smallest T>0, such that f(x + T) = f(x) is called: "),
		3, QStringLiteral("(A) cubic"),
		QStringLiteral("(B) quasi-periodic"),
		QStringLiteral("(C) infinite"),
		QStringLiteral("(D) periodic"),1,3);

	questions_easyEN[2].setInfo(QStringLiteral("Let A ~ B be matrices over a vector space V, then there exists a matrix P such that B = P^(-1)AP. How is this matrix called? "),
		2, QStringLiteral("(A) real matrix"),
		QStringLiteral("(B) inverse matrix"),
		QStringLiteral("(C) transition matrix"),
		QStringLiteral("(D) product matrix"),2,3);

	questions_easyEN[3].setInfo(QStringLiteral("Let Q be a matrix over a vector space V and Q' its transpose. If Q'Q = Q Q' = I then matrix Q is called: "),
		0, QStringLiteral("(A) orthogonal"),
		QStringLiteral("(B) inverse"),
		QStringLiteral("(C) transition matrix"),
		QStringLiteral("(D) product matrix"),0,2);

	questions_easyEN[4].setInfo(QStringLiteral("Let (G,*) be a group. An element e of G such that e*x=x*e=x for any x from G is called: "),
		3, QStringLiteral("(A) inverse"),
		QStringLiteral("(B) null"),
		QStringLiteral("(C) multiplicative"),
		QStringLiteral("(D) neutral"),0,3);

	questions_easyEN[5].setInfo(QStringLiteral("Let f: M --> N be a map. We say that f is injective if:"),
		0, QStringLiteral("(A) any two distinct elements of M are mapped onto two distinct elements of N."),
		QStringLiteral("(B) any two distinct elements of M are mapped onto a single element of N."),
		QStringLiteral("(C) every element of M is mapped onto every element of N."),
		QStringLiteral("(D) every element of M is mapped onto a countable subset of N."),0,2);

	questions_easyEN[6].setInfo(QStringLiteral("At least twice differentiable function f: M --> R has a local maximum in at point p of M if: "),
		3, QStringLiteral("(A) for x->p , f(x) -> 0 "),
		QStringLiteral("(B) has a zero derivative at point p i.e.: f'(p) == 0"),
		QStringLiteral("(C) f'(x) > 0 for x < p & f'(x) < 0 for x > p & f''(p) == 0"),
		QStringLiteral("(D) f'(p) == 0 & f''(p) < 0"),1,2);

	questions_easyEN[7].setInfo(QStringLiteral("Let V,W be vector spaces. A map A:V-->W is linear if: "),
		1, QStringLiteral("(A) any vector v from V is mapped onto a straight line passing through the origin"),
		QStringLiteral("(B) for any two vectors u,v of V and two scalars a,b : A(au+bv)=aA(u)+bA(v)."),
		QStringLiteral("(C) for any two vectors u,v of V and two scalars a,b : A(au+bv)=abA(u)A(v)."),
		QStringLiteral("(D) there exists an inverse map A^(-1):W-->V such that AA^(-1)=A^(-1)A=I."),0,1);

	questions_easyEN[8].setInfo(QStringLiteral("Let f and g be differentiable functions and g!=0 , g'!=0, then lim_{x->a} f(x)/g(x) = lim_{x->a} f'(x)/g'(x). This rule is called: "),
		2, QStringLiteral("(A) quotient rule"),
		QStringLiteral("(B) limit rule"),
		QStringLiteral("(C) L'Hospital rule"),
		QStringLiteral("(D) L'Hostinsky rule"),0,2);

	questions_easyEN[9].setInfo(QStringLiteral("Let f(x)=x*sin(x). What is the anti-derivative of f? "),
		0, QStringLiteral("(A) sin(x)-x*cos(x) + C"),
		QStringLiteral("(B) it does not exist"),
		QStringLiteral("(C) cos(x) + C"),
		QStringLiteral("(D) 2*sin(x)*cos(x) + C"),0,2);

	// ================================== slovak questions medium ===================================

	questions_mediumSK[0].setInfo(QStringLiteral("Množina X je spočítateľná, ak: "),
		2, QStringLiteral("(A) je X totožná s množinou prirodzených čísel N."),
		QStringLiteral("(B) existuje injekcia f: N --> X, pričom N je množina prirodzených čísel."),
		QStringLiteral("(C) existuje bijekcia f: N --> X, pričom N je množina prirodzených čísel."),
		QStringLiteral("(D) sa dá zobraziť na množinu prirodzených čísel N."),2,3);

	questions_mediumSK[1].setInfo(QStringLiteral("Nech je x_n Cauchyovská postupnosť v nejakom metrickom priestore X. Potom ak je X úplný metrický priestor: "),
		1, QStringLiteral("(A) x_n diverguje."),
		QStringLiteral("(B) x_n konverguje."),
		QStringLiteral("(C) existuje také n0, že pre všetky m>n0, x_{m+1}=x_{m}."),
		QStringLiteral("(D) sú všetky členy x_n od seba rovnako vzdialené."),1,2);

	questions_mediumSK[2].setInfo(QStringLiteral("Obsah plochy pod grafom funkcie exp(-x^2) cez celú reálnu os je:"),
		3, QStringLiteral("(A) Pi^2"),
		QStringLiteral("(B) 1"),
		QStringLiteral("(C) Pi"),
		QStringLiteral("(D) sqrt(Pi)"),2,3);

	questions_mediumSK[3].setInfo(QStringLiteral("Nech je u:R^2-->R^2 vektorové pole. Ak je C:I-->R^2 jednoduchá uzavretá krivka a krivkový integrál druhého druhu z v po C je nulový, tak:"),
		3, QStringLiteral("(A) u je v každom bode nulový vektor."),
		QStringLiteral("(B) u je hladké vektorové pole."),
		QStringLiteral("(C) u nie je v žiadnom bode diferencovateľné."),
		QStringLiteral("(D) u je gradientom nejakej skalárnej funkcie f na R^2."),0,3);

	questions_mediumSK[4].setInfo(QStringLiteral("Nech je F:R^n-->R^n diferencovateľné vektorové pole a je O oblasť s Lipschitzovskou hranicou v R^n, potom integrál div(F) cez O je rovný integrálu F.n po hranici O, pričom n:"),
		0, QStringLiteral("(A) je vektor vonkajšej normály na hranicu oblasti O."),
		QStringLiteral("(B) je vektor vnútornej normály na hranicu oblasti O."),
		QStringLiteral("(C) je konzervatívne vektorové pole v R^n."),
		QStringLiteral("(D) je normálou na rovinu xy."),0,1);

	questions_mediumSK[5].setInfo(QStringLiteral("u(t)=C*exp(t), kde C je reálne, je riešením ODR:"),
		2, QStringLiteral("(A) u''== 1."),
		QStringLiteral("(B) u'== t."),
		QStringLiteral("(C) u'== u."),
		QStringLiteral("(D) u'== u + C."),1,2);

	questions_mediumSK[6].setInfo(QStringLiteral("Nech je V vektorový priestor s bázou x1,...,xn. Ktoré z tvrdení je správne? "),
		1, QStringLiteral("(A) Ľubovoľný vektor v z V sa d  vyjadriť ako nejaká lineárna kombinácia vektorov bázy."),
		QStringLiteral("(B) Ľubovoľný vektor v z V sa dá vyjadriť jednoznačnou lineárnou kombináciou vektorov bázy."),
		QStringLiteral("(C) Všetky vektory bázy sa dajú vyjadriť ako lineárna kombinácia vektora v."),
		QStringLiteral("(D) Ľubovoľný vektor v z V sa dá vyjadriť ako lineárna kombinácia ktoréhokoľvek z vektorov bázy."),0,1);

	questions_mediumSK[7].setInfo(QStringLiteral("Ktoré z tvrdení je správne? "),
		0, QStringLiteral("(A) Nech sú f a g spojité zobrazenia z tej istej množiny, potom aj f/g je spojité, ak g(x)!=0 pre všetky x z def. oboru."),
		QStringLiteral("(B) Nech je f spojité zobrazenie a g je ľubovoľné zobrazenie, potom f+g je tiež spojité."),
		QStringLiteral("(C) Nech sú f a g spojité zobrazenia, potom f*g je spojitá bijekcia."),
		QStringLiteral("(D) Nech je f spojité skoro všade a g je spojité na celom def. obore, potom f+g je tiež spojité."),0,3);

	questions_mediumSK[8].setInfo(QStringLiteral("Nech je f:[0,1]-->R funkcia taká, že: f(x)=1 ak je x racionálne a f(x)=0 inak. Potom Riemannov integrál f cez celý def. obor je:"),
		2, QStringLiteral("(A) 0"),
		QStringLiteral("(B) 1"),
		QStringLiteral("(C) Riemannovský súčet f, s nosičom nulovej miery nekonverguje"),
		QStringLiteral("(D) 1/2"),0,2);

	questions_mediumSK[9].setInfo(QStringLiteral("Nech je F:R-->[0,1] distribučná funkcia náhodnej premennej X. Potom:"),
		3, QStringLiteral("(A) F je periodická."),
		QStringLiteral("(B) F je nerastúca"),
		QStringLiteral("(C) F je rastúca."),
		QStringLiteral("(D) F je neklesajúca."),2,3);

	// ================================== english questions medium ===================================

	questions_mediumEN[0].setInfo(QStringLiteral("A set X is countable if: "),
		2, QStringLiteral("(A) X is equal to the set of positive integers, N."),
		QStringLiteral("(B) there exists an injection f: N --> X, where N are positive integers."),
		QStringLiteral("(C) there exists a bijection f: N --> X, where N are positive integers."),
		QStringLiteral("(D) X can be mapped onto the set N, of positive integers."),2,3);

	questions_mediumEN[1].setInfo(QStringLiteral("Let x_n be a Cauchy sequence in a metric space X. Then if X is complete: "),
		1, QStringLiteral("(A) x_n diverges."),
		QStringLiteral("(B) x_n converges."),
		QStringLiteral("(C) there exists a n0, such that for all m>n0, x_{m+1}=x_{m}."),
		QStringLiteral("(D) all terms x_n are equidistant."),1,2);

	questions_mediumEN[2].setInfo(QStringLiteral("the area under the graph of f(x)=exp(-x^2) through all real numbers is:"),
		3, QStringLiteral("(A) Pi^2"),
		QStringLiteral("(B) 1"),
		QStringLiteral("(C) Pi"),
		QStringLiteral("(D) sqrt(Pi)"),2,3);

	questions_mediumEN[3].setInfo(QStringLiteral("Let u:R^2-->R^2 be a vector field. If C:I-->R^2 is a simple closed curve and the path integral of second kind of v through C is zero, then:"),
		3, QStringLiteral("(A) u is a zero vector at every point."),
		QStringLiteral("(B) u is a smooth vector field."),
		QStringLiteral("(C) u is nowhere differentiable."),
		QStringLiteral("(D) u is a gradient of some scalar function f in R^2."),0,3);

	questions_mediumEN[4].setInfo(QStringLiteral("Let F:R^n-->R^n be a differentiable vector field and M a Lipschitz-boundary region in R^n, then an integral of div(F) through M equals the integral of F.n along the boundary of M, where n:"),
		0, QStringLiteral("(A) is an outward normal vector of the boundary of M."),
		QStringLiteral("(B) is an inward normal vector of the boundary of M."),
		QStringLiteral("(C) is a conservative vector field in R^n."),
		QStringLiteral("(D) is a normal with respect to the xy-plane."),0,1);

	questions_mediumEN[5].setInfo(QStringLiteral("u(t)=C*exp(t),where C is real, solves an ODE:"),
		2, QStringLiteral("(A) u''== 1."),
		QStringLiteral("(B) u'== t."),
		QStringLiteral("(C) u'== u."),
		QStringLiteral("(D) u'== u + C."),1,2);

	questions_mediumEN[6].setInfo(QStringLiteral("Let V be a vector space with basis x1,...,xn. Which one of the statements is true? "),
		1, QStringLiteral("(A) Any v from V can be expressed as any linear combination of the basis vectors."),
		QStringLiteral("(B) Any v from V can be expressed as a unique linear combination of the basis vectors."),
		QStringLiteral("(C) All the basis vectors can be expressed as a linear combination of v."),
		QStringLiteral("(D) Any v from V can be expressed as a linear combination of any one of the basis vectors."),0,1);

	questions_mediumEN[7].setInfo(QStringLiteral("Which one of the statements is true? "),
		0, QStringLiteral("(A) Let f and g be continuous maps from the same domain, then f/g is also continuous, if g(x)!=0 for all x of their domain."),
		QStringLiteral("(B) Let f be continuous and g any map, then f+g is also continuous."),
		QStringLiteral("(C) Let f and g be continuous maps from the same domain, then f*g is a continuous bijection."),
		QStringLiteral("(D) Let f is continuous almost everywhere and g is continuous on their entire domain, then f+g is also continuous."),0,3);

	questions_mediumEN[8].setInfo(QStringLiteral("Let f:[0,1]-->R such that: f(x)=1 if x is rational and f(x)=0 otherwise. Then the Riemann integral of f from 0 to 1 is:"),
		2, QStringLiteral("(A) 0"),
		QStringLiteral("(B) 1"),
		QStringLiteral("(C) Riemann sum of f, with a zero-measure support does not converge."),
		QStringLiteral("(D) 1/2"),0,2);

	questions_mediumEN[9].setInfo(QStringLiteral("Let F:R-->[0,1] be a cumulative distribution function of a random variable X. Then:"),
		3, QStringLiteral("(A) F is periodic."),
		QStringLiteral("(B) F is non-increasing"),
		QStringLiteral("(C) F is increasing."),
		QStringLiteral("(D) F is non-decreasing."),2,3);

	// ================================== slovak questions hard ===================================

	questions_hardSK[0].setInfo(QStringLiteral("Nech f:R-->R je funkcia taká, že f(x)=1/q, ak x=p/q je racionálne a f(x)=0 inak. Potom:"),
		2, QStringLiteral("(A) f je spojitá na všetkých racionálnych x a nespojitá inde."),
		QStringLiteral("(B) f nie je nikde spojitá."),
		QStringLiteral("(C) f je spojitá na všetkých iracionálnych x a nespojitá inde."),
		QStringLiteral("(D) f je všade spojitá."),1,2);

	questions_hardSK[1].setInfo(QStringLiteral("Ktoré z nasledujúcich tvrdení NIE JE pravdivé?"),
		0, QStringLiteral("(A) Ak je f spojitá bijekcia, jej inverzná funkcia je tiež spojitá."),
		QStringLiteral("(B) Ak je f diferencovateľná všade, okrem bodu p, potom je v p spojitá."),
		QStringLiteral("(C) Nech f(x)=sin(1/x) ak x!=0 a f(x)=0 if x=0. Potom f nie je spojitá v x=0."),
		QStringLiteral("(D) Každá bijektívna funkcia má inverznú funkciu."),0,2);

	questions_hardSK[2].setInfo(QStringLiteral("Ktoré z nasledujúcich tvrdení je pravdivé?"),
		0, QStringLiteral("(A) Ak je f invertovateľná, potom jej inverzná funkcia je spojitá."),
		QStringLiteral("(B) Nech je fn(x)=sin(nx) postupnosť funkcii na intervale [a,b], potom fn konverguje v L2[a,b]"),
		QStringLiteral("(C) Nech je C množina komplex. čísel a |.| a komplexný modul, potom (C,|.|) úplný metrický priestor."),
		QStringLiteral("(D) f:]0,infty[-->R taká, že f(x)=1/x je na ]0,infty[ rovnomerne spojitá."),2,3);

	questions_hardSK[3].setInfo(QStringLiteral("Ktor‚ z nasledujúcich tvrdení je pravdivé?"),
		3, QStringLiteral("(A) Okrajová úloha u''(x)=x, u'(0)=u'(1)=0 má rastúce riešenie."),
		QStringLiteral("(B) Riešenie u''(x)==0 s okraj. podm. u(0)=u'(0)=u'(1)=u(1)=0, je u=Ax+B, kde A,B sú reálne."),
		QStringLiteral("(C) Okrajová úloha u''(x)=1, u'(0)=u'(1)=0 má konštantné riešenie."),
		QStringLiteral("(D) Okrajová úloha u''(x)==0 , u(0)=u'(0)=u'(1)=u(1)=0, má konštantné riešenie."),2,3);

	questions_hardSK[4].setInfo(QStringLiteral("Nech je f:[0,1]-->R spojitá. Potom:"),
		1, QStringLiteral("(A) f má v [0,1] lokálny extrém."),
		QStringLiteral("(B) Ak je max(f)<=1 a min(f)>=0, potom má f aspoň jeden pevný bod f(p)=p."),
		QStringLiteral("(C) Ak je max(f)<1 a min(f)>0, potom m  f aspoň jeden pevný bod f(p)=p."),
		QStringLiteral("(D) f nie je nutne rovnomerne spojitá na [0,1]."),1,2);

	questions_hardSK[5].setInfo(QStringLiteral("Ktoré z nasledujúcich tvrdení je pravdivé?"),
		1, QStringLiteral("(A) Množina všetkých podmnožín N (prirodzených č.) je spočítateľná."),
		QStringLiteral("(B) Každá spojitá f:[a,b]-->R je na [a,b] rovnomerne spojitá."),
		QStringLiteral("(C) e^Pi > Pi^e."),
		QStringLiteral("(D) Ak je (.,.) skalárny súčin, potom (x,y) >= 0."),0,1);

	questions_hardSK[6].setInfo(QStringLiteral("Nech je M lineárny podpriestor L, a A,B sú matice nad L, potom:"),
		3, QStringLiteral("(A) Ak M obsahuje vlastný vektor A aj B, potom AB nemá v M vlastné vektory."),
		QStringLiteral("(B) Ak M obsahuje vlastný vektor A aj B, potom obsahuje aj vlastný vektor AB, ale nie BA."),
		QStringLiteral("(C) Ak M obsahuje vlastný vektor A, potom obsahuje aj vlastý vektor AB a BA."),
		QStringLiteral("(D) Ak M obsahuje vlastný vektor A aj B, potom obsahuje aj vlastný vektor AB a BA."),2,3);

	questions_hardSK[7].setInfo(QStringLiteral("Z ktorej z týchto množín NEMOŽNO skonštruovať topologickú varietu?"),
		2, QStringLiteral("(A) Mobiova páska."),
		QStringLiteral("(B) Torus."),
		QStringLiteral("(C) Kružnica spojená s priamkou v jednom bode."),
		QStringLiteral("(D) Sféra bez severného pólu."),2,3);

	questions_hardSK[8].setInfo(QStringLiteral("Nech je f:[0,infty[-->R sub-aditívna, zprava-diferencovateľná v 0 a f(0)=0. Ktoré z nasledujúcich tvrdení je pravdivé?"),
		2, QStringLiteral("(A) f(x)<=f'(0) pre všetky x in [0,infty[."),
		QStringLiteral("(B) f(x)<=xf'(x) pre všetky x in [0,infty[."),
		QStringLiteral("(C) f(x)<=xf'(0) pre všetky x in [0,infty[."),
		QStringLiteral("(D) f(x)<=f'(x) pre všetky x in [0,infty[."),0,2);

	questions_hardSK[9].setInfo(QStringLiteral("Nech je C:I-->R^2 jednoduchá uzavretá krivka, L(C) jej dĺžka a A(C) obsah oblasti ohraničenej krivkou C, potom:"),
		1, QStringLiteral("(A)  A(C) <= L(C)^2."),
		QStringLiteral("(B) A(C) <= L(C)^2 / (4*Pi)."),
		QStringLiteral("(C)  A(C) <= L(C) / (4*Pi)."),
		QStringLiteral("(D)  A(C) = L(C)"),1,2);


	// ================================== english questions hard ===================================

	questions_hardEN[0].setInfo(QStringLiteral("Let f:R-->R such that f(x)=1/q, if x=p/q is rational and f(x)=0 otherwise. Then:"),
		2, QStringLiteral("(A) f is continuous in all rational x and not continuous elsewhere."),
		QStringLiteral("(B) f is nowhere continuous."),
		QStringLiteral("(C) f is continuous in all irrational x and not continuous elsewhere."),
		QStringLiteral("(D) f is everywhere continuous."),1,2);

	questions_hardEN[1].setInfo(QStringLiteral("Which one of these statements is NOT true?"),
		0, QStringLiteral("(A) If f is a continuous bijection, then its inverse is also continuous"),
		QStringLiteral("(B) If f is differentiable everywhere, except for one point p, then it's continuous at p."),
		QStringLiteral("(C) Let f(x)=sin(1/x) if x!=0 and f(x)=0 if x=0. Then f is not continuous at x=0."),
		QStringLiteral("(D) Every bijective function has an inverse."),0,2);

	questions_hardEN[2].setInfo(QStringLiteral("Which one of these statements is true?"),
		0, QStringLiteral("(A) If f is invertible, then its inverse is continuous"),
		QStringLiteral("(B) Let fn(x)=sin(nx) be a sequence of functions over interval [a,b], then fn converges in L2[a,b]"),
		QStringLiteral("(C) Let C be complex numbers and |.| a complex module, then (C,|.|) is a complete metric space."),
		QStringLiteral("(D) f:]0,infty[-->R, such that f(x)=1/x is uniformly continuous on ]0,infty[."),2,3);

	questions_hardEN[3].setInfo(QStringLiteral("Which one of these statements is true?"),
		3, QStringLiteral("(A) A boundary-value problem u''(x)=x, u'(0)=u'(1)=0 has an increasing solution."),
		QStringLiteral("(B) The solution of u''(x)==0 with boundary conditions u(0)=u'(0)=u'(1)=u(1)=0, is u=Ax+B, where A,B are real."),
		QStringLiteral("(C) A boundary-value problem u''(x)=1, u'(0)=u'(1)=0 has a constant solution."),
		QStringLiteral("(D) A boundary-value problem u''(x)==0 , u(0)=u'(0)=u'(1)=u(1)=0, has a constant solution."),2,3);

	questions_hardEN[4].setInfo(QStringLiteral("Let f:[0,1]-->R be continuous. Then:"),
		1, QStringLiteral("(A) f has a local extreme in [0,1]."),
		QStringLiteral("(B) If max(f)<=1 and min(f)>=0, then f has at least one fixed point f(p)=p."),
		QStringLiteral("(C) If max(f)<1 and min(f)>0, then f has at least one fixed point f(p)=p."),
		QStringLiteral("(D) f is not necessarily uniformly continuous on [0,1]."),1,2);

	questions_hardEN[5].setInfo(QStringLiteral("Which one of these statements is true?"),
		1, QStringLiteral("(A) The set of all subsets of N (positive int) is countable."),
		QStringLiteral("(B) Every continuous f:[a,b]-->R is also uniformly continuous."),
		QStringLiteral("(C) e^Pi > Pi^e."),
		QStringLiteral("(D) if (.,.) is an inner product, then (x,y) >= 0."),0,1);

	questions_hardEN[6].setInfo(QStringLiteral("Let M be a linear subspace of L, and A,B are matrices on L, then:"),
		3, QStringLiteral("(A) If M contains an eigenvector of both A and B, then AB has no eigenvectors in M."),
		QStringLiteral("(B) If M contains an eigenvector of both A and B, then it contains an eigenvector of AB, but not BA."),
		QStringLiteral("(C) If M contains an eigenvector of A, then it contains an eigenvector of AB and BA too."),
		QStringLiteral("(D) If M contains an eigenvector of both A and B, then it contains an eigenvector of AB and BA too."),2,3);

	questions_hardEN[7].setInfo(QStringLiteral("Which one of these sets canNOT be thought of as a topological manifold?"),
		2, QStringLiteral("(A) A Mobius strip."),
		QStringLiteral("(B) A torus."),
		QStringLiteral("(C) A circle with a straight line attached at one point."),
		QStringLiteral("(D) A sphere without its north pole."),2,3);

	questions_hardEN[8].setInfo(QStringLiteral("Let f:[0,infty[-->R be subadditive, right-differentiable at 0 and f(0)=0. Which one of the statements is true?"),
		2, QStringLiteral("(A) f(x)<=f'(0) for any x in [0,infty[."),
		QStringLiteral("(B) f(x)<=xf'(x) for any x in [0,infty[."),
		QStringLiteral("(C) f(x)<=xf'(0) for any x in [0,infty[."),
		QStringLiteral("(D) f(x)<=f'(x) for any x in [0,infty[."),0,2);

	questions_hardEN[9].setInfo(QStringLiteral("Let C:I-->R^2 be a simple closed curve, L(C) its length and A(C) the are of the region bounded by C, then:"),
		1, QStringLiteral("(A)  A(C) <= L(C)^2."),
		QStringLiteral("(B) A(C) <= L(C)^2 / (4*Pi)."),
		QStringLiteral("(C)  A(C) <= L(C) / (4*Pi)."),
		QStringLiteral("(D)  A(C) = L(C)"),0,2);

}

void Milionar::ActionNewGame()
{
	if (ui.lineEdit->text() != QStringLiteral(""))
	{
		game_started = true;
		playing.NullScore();
		qDebug() << "Player NullScore()";
		int indices[10] = {0,1,2,3,4,5,6,7,8,9};

		if (ui.checkBox->isChecked())
		{
			std::random_shuffle(std::begin(indices), std::end(indices));
		}
		for (int i = 0; i < 10; i++) game_indices[i] = indices[i];

		ui.pushButton_4->setEnabled(false);
		ui.pushButton_5->setEnabled(true);

		// 50:50 options /w respect to difficulty
		if (ui.comboBox->currentIndex() == 0)
		{
			ui.pushButton->setEnabled(true);
			ui.pushButton_2->setEnabled(true);
			ui.pushButton_3->setEnabled(true);
		}
		else if (ui.comboBox->currentIndex() == 1)
		{
			ui.pushButton->setEnabled(true);
			ui.pushButton_2->setEnabled(true);
			ui.pushButton_3->setVisible(false);
		}
		else if (ui.comboBox->currentIndex() == 2)
		{
			ui.pushButton->setEnabled(true);
			ui.pushButton_2->setVisible(false);
			ui.pushButton_3->setVisible(false);
		}

		playing.setPlayer(ui.lineEdit->text());

		ui.pushButton_6->setEnabled(true);
		ui.pushButton_7->setEnabled(true);

		ui.radioButton->setEnabled(true);
		ui.radioButton_2->setEnabled(true);
		ui.radioButton_3->setEnabled(true);
		ui.radioButton_4->setEnabled(true);

		ui.comboBox->setEnabled(false);

		current = game_indices;
		if (ui.comboBox_2->currentIndex() == 0)
		{
			if (ui.comboBox->currentIndex() == 0)
			{
				askQuestion(questions_easySK[*current]);
			}
			else if (ui.comboBox->currentIndex() == 1)
			{
				askQuestion(questions_mediumSK[*current]);
			}
			else
			{
				askQuestion(questions_hardSK[*current]);
			}
		}
		else
		{
			if (ui.comboBox->currentIndex() == 0)
			{
				askQuestion(questions_easyEN[*current]);
			}
			else if (ui.comboBox->currentIndex() == 1)
			{
				askQuestion(questions_mediumEN[*current]);
			}
			else
			{
				askQuestion(questions_hardEN[*current]);
			}
		}		
	}
	else
	{
//		qDebug() << "error";
		QMessageBox msgBox;
		if (ui.comboBox_2->currentIndex() == 0) msgBox.setText(QStringLiteral("Zadajte meno, prosím"));
		else msgBox.setText(QStringLiteral("Please enter your name"));	
		msgBox.exec();
	}
}

void Milionar::ActionConfirm()
{
	if (ui.comboBox_2->currentIndex() == 0)
	{
		if (ui.comboBox->currentIndex() == 0)
		{
			evaluateQuestion(questions_easySK[*current]);
		}
		else if (ui.comboBox->currentIndex() == 1)
		{
			evaluateQuestion(questions_mediumSK[*current]);
		}
		else if (ui.comboBox->currentIndex() == 2)
		{
			evaluateQuestion(questions_hardSK[*current]);
		}
	}
	else
	{
		if (ui.comboBox->currentIndex() == 0)
		{
			evaluateQuestion(questions_easyEN[*current]);
		}
		else if (ui.comboBox->currentIndex() == 1)
		{
			evaluateQuestion(questions_mediumEN[*current]);
		}
		else if (ui.comboBox->currentIndex() == 2)
		{
			evaluateQuestion(questions_hardEN[*current]);
		}
	}
	if (on5050) on5050 = false;

	ui.pushButton->setEnabled(true);
	ui.pushButton_2->setEnabled(true);
	ui.pushButton_3->setEnabled(true);

	current++;
	qDebug() << "here...at ActionConfirm";
	if (current - game_indices >= 9)
	{
		endGame();
	}
	else
	{
		ui.radioButton->setEnabled(true);
		ui.radioButton_2->setEnabled(true);
		ui.radioButton_3->setEnabled(true);
		ui.radioButton_4->setEnabled(true);
		if (ui.comboBox_2->currentIndex() == 0)
		{
			if (ui.comboBox->currentIndex() == 0)
			{
				askQuestion(questions_easySK[*current]);
			}
			else if (ui.comboBox->currentIndex() == 1)
			{
				askQuestion(questions_mediumSK[*current]);
			}
			else
			{
				askQuestion(questions_hardSK[*current]);
			}
		}
		else
		{
			if (ui.comboBox->currentIndex() == 0)
			{
				askQuestion(questions_easyEN[*current]);
			}
			else if (ui.comboBox->currentIndex() == 1)
			{
				askQuestion(questions_mediumEN[*current]);
			}
			else
			{
				askQuestion(questions_hardEN[*current]);
			}
		}
	}
}

void Milionar::ActionSkip()
{
	playing.scoreUpdate(0.5);
	ui.doubleSpinBox->setValue(playing.Score());
	if (on5050) on5050 = false;

	ui.pushButton->setEnabled(true);
	ui.pushButton_2->setEnabled(true);
	ui.pushButton_3->setEnabled(true);

	current++;
	qDebug() << "here...at ActionSkip";
	if (current - game_indices >= 9)
	{
		endGame();
	}
	else
	{
		ui.radioButton->setEnabled(true);
		ui.radioButton_2->setEnabled(true);
		ui.radioButton_3->setEnabled(true);
		ui.radioButton_4->setEnabled(true);
		if (ui.comboBox_2->currentIndex() == 0)
		{
			if (ui.comboBox->currentIndex() == 0)
			{
				askQuestion(questions_easySK[*current]);
			}
			else if (ui.comboBox->currentIndex() == 1)
			{
				askQuestion(questions_mediumSK[*current]);
			}
			else
			{
				askQuestion(questions_hardSK[*current]);
			}
		}
		else
		{
			if (ui.comboBox->currentIndex() == 0)
			{
				askQuestion(questions_easyEN[*current]);
			}
			else if (ui.comboBox->currentIndex() == 1)
			{
				askQuestion(questions_mediumEN[*current]);
			}
			else
			{
				askQuestion(questions_hardEN[*current]);
			}
		}
	}
}

void Milionar::ActionEndGame()
{
	qDebug() << "here...at ActionEndGame";
	int remaining = &game_indices[9] - current + 1;
	qDebug() << remaining;
	for (int i = 0; i < remaining; i++) playing.scoreUpdate(0.5);
	ui.doubleSpinBox->setValue(playing.Score());

	endGame();
}

void Milionar::Action5050()
{
	qDebug() << "here...at Action5050";
	QPushButton* button = qobject_cast<QPushButton*>(sender());
	button->setVisible(false);
	on5050 = true;

	ui.pushButton->setEnabled(false);
	ui.pushButton_2->setEnabled(false);
	ui.pushButton_3->setEnabled(false);

	if (ui.comboBox_2->currentIndex() == 0)
	{
		if (ui.comboBox->currentIndex() == 0)
		{
			askQuestion5050(questions_easySK[*current]);
		}
		else if (ui.comboBox->currentIndex() == 1)
		{
			askQuestion5050(questions_mediumSK[*current]);
		}
		else
		{
			askQuestion5050(questions_hardSK[*current]);
		}
	}
	else
	{
		if (ui.comboBox->currentIndex() == 0)
		{
			askQuestion5050(questions_easyEN[*current]);
		}
		else if (ui.comboBox->currentIndex() == 1)
		{
			askQuestion5050(questions_mediumEN[*current]);
		}
		else
		{
			askQuestion5050(questions_hardEN[*current]);
		}
	}
}

Question Milionar::Questions_EasySK(int i)
{
	return questions_easySK[i];
}

Question Milionar::Questions_MediumSK(int i)
{
	return questions_mediumSK[i];
}

Question Milionar::Questions_HardSK(int i)
{
	return questions_hardSK[i];
}

Question Milionar::Questions_EasyEN(int i)
{
	return questions_easyEN[i];
}

Question Milionar::Questions_MediumEN(int i)
{
	return questions_mediumEN[i];
}

Question Milionar::Questions_HardEN(int i)
{
	return questions_hardEN[i];
}

void Milionar::askQuestion(Question &Q)
{
	qDebug() << "here...at askQuestion";
	ui.radioButton->setEnabled(true);
	ui.radioButton_2->setEnabled(true);
	ui.radioButton_3->setEnabled(true);
	ui.radioButton_4->setEnabled(true);

	QString QuestionText = Q.Content() + "\n" + Q.A() + "\n" + Q.B() + "\n" + Q.C() + "\n" + Q.D();
	qDebug() << QuestionText;
	ui.QuestionField->setText(QuestionText);
}

void Milionar::askQuestion5050(Question &Q)
{
	ui.radioButton->setEnabled(false);
	ui.radioButton_2->setEnabled(false);
	ui.radioButton_3->setEnabled(false);
	ui.radioButton_4->setEnabled(false);

	bool opt[4];
	for (int i = 0; i < 4; i++)
	{
		if (i == Q.fiftyFifty(0) || i == Q.fiftyFifty(1) ) opt[i] = true;
		else opt[i] = false;
	}
	for (int i = 0; i < 4; i++) qDebug() << opt[i];

	QString QuestionText = Q.Content();
	if (opt[0])
	{
		QuestionText.append("\n");
		QuestionText.append(Q.A());
		ui.radioButton->setEnabled(true);
	}	 
	if (opt[1])
	{
		QuestionText.append("\n");
		QuestionText.append(Q.B());
		ui.radioButton_2->setEnabled(true);
	}		
	if (opt[2])
	{
		QuestionText.append("\n");
		QuestionText.append(Q.C());
		ui.radioButton_3->setEnabled(true);
	}
	if (opt[3])
	{
		QuestionText.append("\n");
		QuestionText.append(Q.D());
		ui.radioButton_4->setEnabled(true);
	}
	qDebug() << "here...at askQuestion5050";
	qDebug() << QuestionText;
	ui.QuestionField->setText(QuestionText);
}

void Milionar::evaluateQuestion(Question &Q)
{
	int a;
	if (ui.radioButton->isChecked()) a = 0;
	else if (ui.radioButton_2->isChecked()) a = 1;
	else if (ui.radioButton_3->isChecked()) a = 2;
	else if (ui.radioButton_4->isChecked()) a = 3;

	if (a == Q.Anwser())
	{
		playing.scoreUpdate(1.);
	}
	ui.doubleSpinBox->setValue(playing.Score());
	qDebug() << "here...at EvaluateQuestion";
}

void Milionar::endGame()
{
	game_started = false;
	ui.pushButton_4->setEnabled(true);
	ui.pushButton_5->setEnabled(false);

	ui.pushButton->setVisible(true);
	ui.pushButton_2->setVisible(true);
	ui.pushButton_3->setVisible(true);

	ui.pushButton->setEnabled(false);
	ui.pushButton_2->setEnabled(false);
	ui.pushButton_3->setEnabled(false);

	ui.radioButton->setEnabled(false);
	ui.radioButton_2->setEnabled(false);
	ui.radioButton_3->setEnabled(false);
	ui.radioButton_4->setEnabled(false);

	ui.pushButton_6->setEnabled(false);
	ui.pushButton_7->setEnabled(false);

	ui.comboBox->setEnabled(true);
	ui.QuestionField->setText(QStringLiteral(""));

	qDebug() << "here...at endGame";

//	current = NULL;
}



void Milionar::ActionChangeLanguage()
{
	int L = ui.comboBox_2->currentIndex();
//	qDebug() << "changing language";
	if (L == 0)
	{
		ui.label_4->setText(QStringLiteral("Jazyk:"));
		ui.pushButton_8->setText(QStringLiteral("Zmeň jazyk"));
		ui.label->setText(QStringLiteral("Meno:"));
		ui.label_2->setText(QStringLiteral("Skóre:"));
		ui.label_3->setText(QStringLiteral("Náročnosť:"));
		ui.comboBox->setItemText(0, QStringLiteral("Ľahká"));
		ui.comboBox->setItemText(1, QStringLiteral("Stredná"));
		ui.comboBox->setItemText(2, QStringLiteral("Ťažká"));
		ui.checkBox->setText(QStringLiteral("Náhodne"));
		ui.pushButton_4->setText(QStringLiteral("Nová hra"));
		ui.pushButton_5->setText(QStringLiteral("Ukončiť hru"));
		ui.groupBox->setTitle(QStringLiteral("Otázka:"));
		ui.pushButton_6->setText(QStringLiteral("Potvrdiť"));
		ui.pushButton_7->setText(QStringLiteral("Preskočiť otázku"));
		if (game_started)
		{
			if (on5050)
			{
				if (ui.comboBox->currentIndex() == 0)
				{
					askQuestion5050(questions_easySK[*current]);
				}
				else if (ui.comboBox->currentIndex() == 1)
				{
					askQuestion5050(questions_mediumSK[*current]);
				}
				else
				{
					askQuestion5050(questions_hardSK[*current]);
				}
			}
			else
			{
				if (ui.comboBox->currentIndex() == 0)
				{
					askQuestion(questions_easySK[*current]);
				}
				else if (ui.comboBox->currentIndex() == 1)
				{
					askQuestion(questions_mediumSK[*current]);
				}
				else
				{
				askQuestion(questions_hardSK[*current]);
				}
			}
		}
	}
	else
	{
		ui.label_4->setText(QStringLiteral("Language:"));
		ui.pushButton_8->setText(QStringLiteral("Change language"));
		ui.label->setText(QStringLiteral("Name:"));
		ui.label_2->setText(QStringLiteral("Score:"));
		ui.label_3->setText(QStringLiteral("Difficulty:"));
		ui.comboBox->setItemText(0, QStringLiteral("Easy"));
		ui.comboBox->setItemText(1, QStringLiteral("Medium"));
		ui.comboBox->setItemText(2, QStringLiteral("Hard"));
		ui.checkBox->setText(QStringLiteral("Random"));
		ui.pushButton_4->setText(QStringLiteral("New game"));
		ui.pushButton_5->setText(QStringLiteral("End game"));
		ui.groupBox->setTitle(QStringLiteral("Question:"));
		ui.pushButton_6->setText(QStringLiteral("Confirm"));
		ui.pushButton_7->setText(QStringLiteral("Skip Question"));
		if (game_started)
		{
			if (on5050)
			{
				if (ui.comboBox->currentIndex() == 0)
				{
					askQuestion5050(questions_easyEN[*current]);
				}
				else if (ui.comboBox->currentIndex() == 1)
				{
					askQuestion5050(questions_mediumEN[*current]);
				}
				else
				{
					askQuestion5050(questions_hardEN[*current]);
				}
			}
			else
			{
				if (ui.comboBox->currentIndex() == 0)
				{
					askQuestion(questions_easyEN[*current]);
				}
				else if (ui.comboBox->currentIndex() == 1)
				{
					askQuestion(questions_mediumEN[*current]);
				}
				else
				{
					askQuestion(questions_hardEN[*current]);
				}
			}
		}
	}
}
