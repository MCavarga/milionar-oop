#pragma once

#include "Milionar.h"
#include <iostream>
#include <string>
#include <QString>

class Question
{
private:
	QString content;
	QString *options;
	int anwser;
public:
	Question();
	~Question();
	void setInfo(QString c, int a,QString A, QString B, QString C, QString D);
	QString Content();
	int Anwser();
	QString A();
	QString B();
	QString C();
	QString D();
};

