#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_Milionar.h"

class Player
{
private:
	QString name;
	float score = 0.0;
public:
	Player();
	void setPlayer(QString n);
	~Player();
	void scoreUpdate(float change);
	float Score();
	void NullScore();
};

class Question
{
private:
	QString content;
	QString *options;
	int anwser;
	int fiftyFifty_indices[2];
public:
	Question();
	~Question();
	void setInfo(QString c, int a, QString A, QString B, QString C, QString D,int ff1,int ff2);
	QString Content();
	int Anwser();
	QString A();
	QString B();
	QString C();
	QString D();
	int fiftyFifty(int i);
};

class Milionar : public QMainWindow
{
	Q_OBJECT

public:
	Milionar(QWidget *parent = Q_NULLPTR);

private:
	Player playing;

	Ui::MilionarClass ui;

	Question questions_easySK[10];
	Question questions_mediumSK[10];
	Question questions_hardSK[10];

	Question questions_easyEN[10];
	Question questions_mediumEN[10];
	Question questions_hardEN[10];

	int game_indices[10];
	int *current = NULL;
	bool game_started = false;
	bool on5050 = false;

public slots:
	void ActionChangeLanguage();
	void ActionNewGame();
	void ActionConfirm();
	void ActionSkip();
	void ActionEndGame();
	void Action5050();

public:
	Question Questions_EasySK(int i);
	Question Questions_MediumSK(int i);
	Question Questions_HardSK(int i);

	Question Questions_EasyEN(int i);
	Question Questions_MediumEN(int i);
	Question Questions_HardEN(int i);

	void askQuestion(Question &Q);
	void askQuestion5050(Question &Q);
	void evaluateQuestion(Question &Q);
	void endGame();

};
